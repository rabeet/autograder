#include <stdlib.h>
#include <stdio.h>


int main( int argc, char *argv[] ) {
  
  int i, n;
  
  if( argc < 2 ) {
    printf( "Please input param\n" );
    return 1;
  }
  
  n = atoi( argv[1] );
  
  printf( "testing!... testing!... 1!... 2!... 3!\n" );
  
  for( i = 0; i < n; i++ )
    printf( "line\n" );
  
  return 0;
  
}