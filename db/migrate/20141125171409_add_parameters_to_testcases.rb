class AddParametersToTestcases < ActiveRecord::Migration
  def change
    add_column :testcases, :parameters, :text
  end
end
