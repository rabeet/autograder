class AddTestCasesToAssignment < ActiveRecord::Migration
  def change
    add_column :assignments, :test_case, :text
  end
end
