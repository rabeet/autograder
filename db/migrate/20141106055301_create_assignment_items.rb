class CreateAssignmentItems < ActiveRecord::Migration
  def change
    create_table :assignment_items do |t|
      t.text :submission
      t.integer :assignedgrade
      t.references :user, index: true
      t.references :assignment, index: true

      t.timestamps null: false
    end
  end
end
