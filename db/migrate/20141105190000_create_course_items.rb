class CreateCourseItems < ActiveRecord::Migration
  def change
    create_table :course_items do |t|
      t.integer :grade
      t.references :user, index: true
      t.references :course, index: true

      t.timestamps null: false
    end
  end
end
