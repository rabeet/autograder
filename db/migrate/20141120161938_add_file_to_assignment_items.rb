class AddFileToAssignmentItems < ActiveRecord::Migration
  def change
    add_column :assignment_items, :file, :string
  end
end
