class CreateTestcases < ActiveRecord::Migration
  def change
    create_table :testcases do |t|
      t.text :params
      t.text :pattern
      t.references :assignment, index: true

      t.timestamps null: false
    end
  end
end
