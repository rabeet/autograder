class AddResultsToAssignmentItem < ActiveRecord::Migration
  def change
    add_column :assignment_items, :results, :text
  end
end
