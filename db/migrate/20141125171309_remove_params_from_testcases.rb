class RemoveParamsFromTestcases < ActiveRecord::Migration
  def change
    remove_column :testcases, :params, :text
  end
end
