class CreateAssignments < ActiveRecord::Migration
  def change
    create_table :assignments do |t|
      t.date :due
      t.integer :points
      t.text :content
      t.text :submission

      t.timestamps null: false
    end
  end
end
