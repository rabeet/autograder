#include <stdlib.h>
#include <stdio.h>

int main( int argc, char *argv[] ) {
  int i, n;
  n = argc >= 2 ? atoi( argv[1] ) : 1;
  for( i = 0; i < n; i++)
    printf( "hello " );
  printf( "world.\n\n" );
  return 0;
}