json.array!(@assignments) do |assignment|
  json.extract! assignment, :id, :due, :points, :content, :submission, :name
  json.url assignment_url(assignment, format: :json)
end
