json.array!(@testcases) do |testcase|
  json.extract! testcase, :id, :parameters, :pattern
  json.url assignment_testcase_url(@assignment, testcase, format: :json)
end
