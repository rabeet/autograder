class Course < ActiveRecord::Base
   belongs_to :admin
   has_many :users
   has_many :assignments
   has_many :course_items
   
   validates :name, presence: true
end
