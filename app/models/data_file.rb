class DataFile < ActiveRecord::Base
  attr_accessor :upload
  def self.save_file(upload)   
    file_name = upload['datafile'].original_filename  if  (upload['datafile'] !='')    
    file = upload['datafile'].read    

    file_type = file_name.split('.').last
    new_name_file = Time.now.to_i
    name_folder = Date.today
    new_file_name_with_type = "#{new_name_file}." + file_type

    image_root = "CPP_FILES"
    command = "#{image_root}" + "/" + "#{name_folder}"

    system 'mkdir', '-p', command # Create new dir
    command_with_file = command + "/" + new_file_name_with_type
    File.open(command_with_file, "wb")  do |f|  
      f.write(file) 
    end
    return IO.binread(command_with_file)
  end
end