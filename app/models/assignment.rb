class Assignment < ActiveRecord::Base
   belongs_to :course
   has_one :course
   belongs_to :admin
   has_many :assignment_items, dependent: :destroy
   has_many :testcases
   
   validates :name, presence: true, length: {maximum:50}
   validates :content, presence: true
   
end
 