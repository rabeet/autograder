class AssignmentItem < ActiveRecord::Base
  belongs_to :user
  belongs_to :assignment

  validates :user_id, presence: true
  validates :assignment_id, presence: true

end
