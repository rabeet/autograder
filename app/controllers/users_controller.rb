class UsersController < ApplicationController
  before_action :admin_user, only: [:index, :destroy, :edit]
  before_action :set_user, only: [:edit, :update, :destroy]

  def index
    if logged_in?
      @users = User.all
    else
      redirect_to login_url
    end
  end
  
  def edit
  end
  
  def update
    respond_to do |format|
      if @user.save
        format.html { redirect_to users_path, notice: 'User was successfully updated.' }
        format.json { render :show, status: :created, location: @user }
      else
        format.html { render :new }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def show
    @user = User.find_by(id:params[:id])
    if (@user == nil || session[:user_id] != @user.id)
      # if user can't be loaded, redirect to login page
      redirect_to login_url
    else
      if isAdmin?
        redirect_to assignments_path
      end
      #user gets loaded so why not load his assignment_items?
      @user_assignments = user_assignments.paginate(page: params[:page])
    end
  end
  
  def destroy
    @user = User.find_by(id:params[:id])
    @user.destroy
    redirect_to users_url, :notice => "User successfully destroyed!"
  end
  
  def new
    @user = User.new
  end
  
  def create
    @user = User.new(user_params)
    if @user.save
      redirect_to @user, :notice => "Account successfully created! Please login."
    else
      render 'new'
    end
  end
  
  private
  
  def check_admin_login
    # If a non-admin user is logged in, then he doesn't need another account
    if logged_in?
      if !isAdmin?
        redirect_to root_path
      end
    end
  end
  
  def set_user
      @user = User.find_by(id:params[:id])
      if @user.nil?
        redirect_to root_path
      end
    end
  
  def user_params
    params.require(:user).permit(:name, :email, :password,
                                 :password_confirmation)
  end
end