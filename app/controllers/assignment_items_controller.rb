class AssignmentItemsController < ApplicationController
  
  # before_action :admin_user, only: index # Only admin can view all submissions, student should only see their submissions
  
  def submit
    if logged_in?
        @current_assignment = AssignmentItem.find_by(assignment_id:params[:id], user_id: current_user.id)
        #system("echo "+@current_assignment.file+" >> log.txt")
        if (@current_assignment.nil?)
          redirect_to root_path
        end
    else
      redirect_to login_url
    end
  end
  
  def show
    @current_assignment = AssignmentItem.find(params[:id])
    @user = User.find(@current_assignment.user_id)
    if !@current_assignment.results.nil?
      @results = ActiveSupport::JSON.decode( @current_assignment.results );
    else
      @results = nil
    end
  end
  
  def trial
    if logged_in?
        @current_assignment = AssignmentItem.find_by(assignment_id:params[:id], user_id: current_user.id)
        #system("echo "+@current_assignment.file+" >> log.txt")
        if (@current_assignment.nil?)
          redirect_to root_path
        end
    else
      redirect_to login_url
    end
        
  end
  
  # method to compile the .cpp file
  def create
    
    # get reference to the current assignment
    @current_assignment = AssignmentItem.find_by(assignment_id:params[:id], user_id: current_user.id)
    tcs = Testcase.where(assignment: params[:id])
    
    testcases = { :num => 0, :tests => [] }
    tcs.each do |tc|
      testcases[:tests][testcases[:num]] = { :params => tc.parameters, :pat => tc.pattern }
      testcases[:num] += 1;
    end
    
    # # retrieve testcases from assignment
    # if @current_assignment.assignment.test_case.nil?
    #   # this is a fake test cases object for testing purposes
    #   testcases = { :num => 3, :tests => [ { :params => "", :pat => "Please input param[\r\n]*" }, { :params => "3", :pat => "(Testing!\.+ +){2}1!\.+ +2!\.+ +3! *([\r\n]+ *line *){3}[\r\n]*" }, { :params => "2", :pat => "([\r\n]*.+)*[\r\n]*" } ] }
    #   #@current_assignment.assignment.test_case = ActiveSupport::JSON.encode( testcases )
    #   #@current_assignment.assignment.save
    # else
    #   testcases = ActiveSupport::JSON.decode( @current_assignment.assignment.test_case )
    #   #@current_assignment.assignment.test_case = nil
    #   #@current_assignment.assignment.save
    # end
    
    # write submissin to file and save it to current assignment
    path = "temp.cpp"
    if params[:upload].blank? # If user used assignment upload then uploaded file = submission otherwise the pasted source code = submission
      # puts 'params[upload] was blank'
      @current_assignment.submission = params[:assignment_item][:submission]
     else
      # puts "params_upload = " + params[:upload].to_s
      d = DataFile.save_file(params[:upload])
      @current_assignment.submission = d
     end
     
    File.open(path, "w+") do |f|
      f.write(@current_assignment.submission)
    end
    # @current_assignment.submission = File.read(path);
    
    # attempt to compile program
    @success = system("g++ temp.cpp -o temp.out 2> temp.log")
    @results = { :compiled => @success, :output => File.read("temp.log"), :test_results => [] }
    if @success
      
      # if successful run test cases and store the results
      i = 0
      while i < testcases[:num]
        testcase = testcases[:tests][i]
        param = testcase[:params]
        pat = testcase[:pat]
      
        system("./temp.out "+param+" 2>&1 > temp.txt")
        output = File.read("temp.txt");
      
        match = /\A#{pat}\Z/.match( output )
        
        if match.nil?
          @results[:test_results][i] = { :test => testcase, :matches => false, :output => output }
        else
          @results[:test_results][i] = { :test => testcase, :matches => true, :output => output }
        end
        
        i += 1
      
      end
      
    else
      
      # if unsuccessful store error message to display to user
      @error_text = @results[:output]
      
    end
      @current_assignment.results = ActiveSupport::JSON.encode( @results );
    
    # save the current assignment
    @current_assignment.save
    
    # delete temporary files
    system("rm temp.cpp")
    system("rm temp.log")
    system("rm temp.out")
    system("rm temp.txt")
    
  end
  
  # method to compile the .cpp file
  def test
    
    @current_assignment = AssignmentItem.find_by(assignment_id:params[:id], user_id: current_user.id)
    
    @assignment_id = params[:id]
    @user_id = current_user.id
    tcs = Testcase.where(assignment: params[:id])
    
    testcases = { :num => 0, :tests => [] }
    tcs.each do |tc|
      testcases[:tests][testcases[:num]] = { :params => tc.parameters, :pat => tc.pattern }
      testcases[:num] += 1;
    end
    
    # write submission to file and save it to current assignment
    path = "temp.cpp"
    
     if params[:upload].blank? # If user used assignment upload then uploaded file = submission otherwise the pasted source code = submission
      # puts 'params[upload] was blank'
      @submission = params[:assignment_item][:submission]
     else
      # puts "params_upload = " + params[:upload].to_s
      d = DataFile.save_file(params[:upload])
      @submission = d
     end
    File.open(path, "w+") do |f|
      f.write(@submission)
    end
    # @current_assignment.submission = File.read(path);
    
    # attempt to compile program
    @success = system("g++ temp.cpp -o temp.out 2> temp.log")
    @results = { :compiled => @success, :output => File.read("temp.log"), :test_results => [] }
    if @success
      
      # if successful run test cases and store the results
      i = 0
      while i < testcases[:num]
        testcase = testcases[:tests][i]
        param = testcase[:params]
        pat = testcase[:pat]
      
        system("./temp.out "+param+" 2>&1 > temp.txt")
        path = "temp.txt"
        output = File.read(path);
      
        match = /\A#{pat}\Z/.match( output )
        
        if match.nil?
          @results[:test_results][i] = { :test => testcase, :matches => false, :output => output }
        else
          @results[:test_results][i] = { :test => testcase, :matches => true, :output => output }
        end
        
        i += 1
      
      end
      
    else
      
      # if unsuccessful store error message to display to user
      @error_text = @results[:output]
      
    end
    
    # delete temporary files
    system("rm temp.cpp")
    system("rm temp.log")
    system("rm temp.out")
    system("rm temp.txt")
    
  end
  
end
