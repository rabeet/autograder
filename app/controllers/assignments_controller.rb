class AssignmentsController < ApplicationController
  before_action :set_assignment, only: [:show, :edit, :update, :destroy, :view_all]
  before_action :admin_user,     only: [:index, :new, :create, :edit, :update, :destroy, :view_all]
  
  def index
    if current_user
      @assignments = Assignment.all
    else
      redirect_to login_url
    end
  end

  # GET /assignments/1
  # GET /assignments/1.json
  def show
  end

  # GET /assignments/new
  def new
    @assignment = Assignment.new
    
    # Assigning to users here
    @all_users = User.where(isadmin: false)
    @assignment_item = AssignmentItem.new
  end
  
  def edit
  end

  # DELETE /assignments/1
  # DELETE /assignments/1.json
  def destroy
    @assignment.destroy
    respond_to do |format|
      format.html { redirect_to assignments_url, notice: 'Assignment was successfully destroyed.' }
      format.json { head :no_content }
    end
  end
  
  def assign
    @assignment = Assignment.find(params[:id])
    @users = User.where(isadmin: false)
  end
  
  def view_all
    @ai = @assignment.assignment_items
  end
  
  def items
  end
  
  def create
    @assignment = Assignment.new(assignment_params)
    @all_users = User.where(isadmin:false)
    @assignment_item = AssignmentItem.new

    respond_to do |format|
      if @assignment.save
        # Creating new assignment item here with the received id as parameter
        params[:users][:id].each do |uid|
          if !uid.empty?
            @ai = AssignmentItem.new(assignment_id:@assignment.id, user_id:uid)
            if @ai.save
              puts "saved for" + @ai.to_s
            else
              puts @ai.errors
            end
          end
        end
        format.html { redirect_to @assignment, notice: 'Assignment was successfully created.' }
        format.json { render :show, status: :created, location: @assignment }
      else
        format.html { render :new }
        format.json { render json: @assignment.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def update

    respond_to do |format|
      if @assignment.save
        format.html { redirect_to @assignment, notice: 'Assignment was successfully edited.' }
        format.json { render :show, status: :created, location: @assignment }
      else
        format.html { render :edit }
        format.json { render json: @assignment.errors, status: :unprocessable_entity }
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_assignment
      @assignment = Assignment.find_by(id:params[:id])
      if @assignment.nil?
        redirect_to root_path
      end
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def assignment_params
      params.require(:assignment).permit(:name, :due, :points, :content, :submission)
    end
end