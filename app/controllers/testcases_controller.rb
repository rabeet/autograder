class TestcasesController < ApplicationController
  def show
    @assignment = Assignment.find_by(id:params[:assignment_id])
    @testcase = Testcase.find_by(id: params[:id])
  end

  def index
    if current_user
      @assignment = Assignment.find_by(id: params[:assignment_id])
      @testcases = Testcase.where(assignment: params[:assignment_id])
    else
      redirect_to login_url
    end
  end

  def indexall
    if current_user
      @testcases = Testcase.all
    else
      redirect_to login_url
    end
  end
  
  def new
    @assignment = Assignment.find_by(id:params[:assignment_id])
    @testcase = Testcase.new(assignment: @assignment)
  end
  
  def edit
    @assignment = Assignment.find_by(id:params[:assignment_id])
    @testcase = Testcase.find_by(id:params[:id])
  end

  # DELETE /assignments/1
  # DELETE /assignments/1.json
  def destroy
    @testcase = Testcase.find_by(id: params[:id])
    aid = @testcase.assignment
    @testcase.destroy
    notice = ""
        
        items = AssignmentItem.where(assignment: aid)
        items.each do |item|
          tcs = Testcase.where(assignment: aid)
          testcases = { :num => 0, :tests => [] }
          tcs.each do |tc|
            testcases[:tests][testcases[:num]] = { :params => tc.parameters, :pat => tc.pattern }
            testcases[:num] += 1;
          end
          
          File.open("temp.cpp", "w+") do |f|
            f.write(item.submission)
          end
          success = system("g++ temp.cpp -o temp.out 2> temp.log")
          results = { :compiled => success, :output => File.read("temp.log"), :test_num => testcases[:num], :test_results => [] }
          
          if success
            i = 0
            while i < testcases[:num]
              system("./temp.out "+testcases[:tests][i][:params]+" 2>&1 > temp.txt")
              tout = File.read("temp.txt");
              match = /\A#{testcases[:tests][i][:pat]}\Z/.match( tout )
              results[:test_results][i] = { :test => testcases[:tests][i], :matches => !match.nil?, :output => tout }
              i += 1
            end
          end
          
          item.results = ActiveSupport::JSON.encode( results );
          if item.save()
            notice = notice + "item #" + item.id.to_s + " saved.\n"
          else
            notice = notice + "item #" + item.id.to_s + " not saved.\n"
          end
        end
        system("rm temp.cpp")
        system("rm temp.log")
        system("rm temp.out")
        system("rm temp.txt")
        
        respond_to do |format|
      format.html { redirect_to assignment_testcases_path(aid), notice: 'Test Case was successfully destroyed.' }
      format.json { head :no_content }
    end
  end
  
  def create
    @assignment = Assignment.find_by(id: params[:assignment_id])
    @testcase = Testcase.new(assignment: @assignment, parameters: params[:testcase][:parameters], pattern: params[:testcase][:pattern])

    respond_to do |format|
      if @testcase.save
        notice = ""
        
        items = AssignmentItem.where(assignment: @assignment.id)
        items.each do |item|
          tcs = Testcase.where(assignment: @testcase.assignment)
          testcases = { :num => 0, :tests => [] }
          tcs.each do |tc|
            testcases[:tests][testcases[:num]] = { :params => tc.parameters, :pat => tc.pattern }
            testcases[:num] += 1;
          end
          
          File.open("temp.cpp", "w+") do |f|
            f.write(item.submission)
          end
          success = system("g++ temp.cpp -o temp.out 2> temp.log")
          results = { :compiled => success, :output => File.read("temp.log"), :test_num => testcases[:num], :test_results => [] }
          
          if success
            i = 0
            while i < testcases[:num]
              system("./temp.out "+testcases[:tests][i][:params]+" 2>&1 > temp.txt")
              tout = File.read("temp.txt");
              match = /\A#{testcases[:tests][i][:pat]}\Z/.match( tout )
              results[:test_results][i] = { :test => testcases[:tests][i], :matches => !match.nil?, :output => tout }
              i += 1
            end
          end
          
          item.results = ActiveSupport::JSON.encode( results );
          if item.save()
            notice = notice + "item #" + item.id.to_s + " saved.\n"
          else
            notice = notice + "item #" + item.id.to_s + " not saved.\n"
          end
        end
        system("rm temp.cpp")
        system("rm temp.log")
        system("rm temp.out")
        system("rm temp.txt")
        
        format.html { redirect_to [@assignment, @testcase], notice: notice + 'Test Case was successfully created.' }
        #format.html { redirect_to '/testcases' }
        format.json { render :show, status: :created, location: [@assignment, @testcase] }
      else
        format.html { render :new }
        format.json { render json: @testcase.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def update
    @assignment = Assignment.find_by(id: params[:assignment_id])
    @testcase = Testcase.find_by(id: params[:id])
    @testcase.assignment = @assignment
    @testcase.parameters = params[:testcase][:parameters]
    @testcase.pattern = params[:testcase][:pattern]
    
    respond_to do |format|
      if @testcase.save
        notice = ""
        
        items = AssignmentItem.where(assignment: @assignment.id)
        items.each do |item|
          tcs = Testcase.where(assignment: @testcase.assignment)
          testcases = { :num => 0, :tests => [] }
          tcs.each do |tc|
            testcases[:tests][testcases[:num]] = { :params => tc.parameters, :pat => tc.pattern }
            testcases[:num] += 1;
          end
          
          File.open("temp.cpp", "w+") do |f|
            f.write(item.submission)
          end
          success = system("g++ temp.cpp -o temp.out 2> temp.log")
          results = { :compiled => success, :output => File.read("temp.log"), :test_num => testcases[:num], :test_results => [] }
          
          if success
            i = 0
            while i < testcases[:num]
              system("./temp.out "+testcases[:tests][i][:params]+" 2>&1 > temp.txt")
              tout = File.read("temp.txt");
              match = /\A#{testcases[:tests][i][:pat]}\Z/.match( tout )
              results[:test_results][i] = { :test => testcases[:tests][i], :matches => !match.nil?, :output => tout }
              i += 1
            end
          end
          
          item.results = ActiveSupport::JSON.encode( results );
          if item.save()
            notice = notice + "item #" + item.id.to_s + " saved.\n"
          else
            notice = notice + "item #" + item.id.to_s + " not saved.\n"
          end
        end
        system("rm temp.cpp")
        system("rm temp.log")
        system("rm temp.out")
        system("rm temp.txt")
        
        format.html { redirect_to [@assignment, @testcase], notice: 'Test Case was successfully created.' }
        format.json { render :show, status: :created, location: @testcase }
      else
        format.html { render :new }
        format.json { render json: @testcase.errors, status: :unprocessable_entity }
      end
    end
  end

  private

    # Never trust parameters from the scary internet, only allow the white list through.
    def testcase_params
      params.require(:testcase).permit(:assignment, :parameters, :pattern)
    end
end
