module AssignmentItemsHelper
  def user_assignments
    @user_assignments ||= current_user.assignment_items
  end
end
