module ApplicationHelper
  def full_title(page_title = '')
    if isAdmin?
      base_title = "Auto Grader II - Admin"
    else
      base_title = "Auto Grader II"
    end
    
    if page_title.empty?
      base_title
    else
      "#{base_title} - #{page_title}"
    end
  end
end