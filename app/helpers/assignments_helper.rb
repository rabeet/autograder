module AssignmentsHelper
  def all_assignments
    @all_assignments ||= Assignment.all
  end
end
