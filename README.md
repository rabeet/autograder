# README #

* Auto Grader II Web interface for C++ programs by Rabeet Fatmi for class Software Architecture class at FGCU.
* Compiles and prints results for C++ programs submitted by students.
* See http://autograder.herokuapp.com
 * * Admin: (username:zalewskiapps@gmail.com, password:password)
 * * User: (username:test@test.com, password:password)