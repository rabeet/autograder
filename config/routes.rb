Rails.application.routes.draw do
  
  #get 'testcases/show'

  get 'testcases' => 'testcases#indexall'

  #get 'testcases/create'

  delete 'testcase/:id/destroy' => 'testcases#destroy'
  
  get 'assignments/:id/assign' => 'assignments#assign'
  
  post 'assignments/:id/assign' => 'assignments#items'

  root 'users#show'
  get 'login' => 'sessions#new'
  post 'login' => 'sessions#create'
  delete 'logout' => 'sessions#destroy'
  # not using every single REST actions
  resources :users
  resources :courses, :only => [:index]
  resources :admins, :only => [:show]
  resources :assignment_items, :only => [:show]
  resources :assignments do
    resources :testcases
  end
  get 'signup' => 'users#new'
  get 'assignments/:id/submissions' => 'assignments#view_all', as: :view_submissions
  get 'assignments/:id/submit' => 'assignment_items#submit', as: :submit
  post 'assignments/:id/submit' => 'assignment_items#create'
  get 'assignments/:id/trial' => 'assignment_items#trial', as: :trial
  post 'assignments/:id/trial' => 'assignment_items#test'

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
